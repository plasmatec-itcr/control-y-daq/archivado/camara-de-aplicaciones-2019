#include <SoftwareSerial.h>
#include <stdio.h>
#include <stdlib.h> 
SoftwareSerial mySerial(5,4);  //rx, tx
SoftwareSerial pump(3,2);
SoftwareSerial presion(1,0);
/*VARIABLES GLOBALES*/
int chn=0; 
int i2c;
volatile float sonda=0; 
bool enviado=false;
unsigned long t1=0;
unsigned long t2=0;
int seleccion=0;
volatile char respuesta_pump[33];
volatile char respuesta[20];
char prueba[10];
String dt2;
String dt1;
String dtsend;
/*Equipo*/
String a="a";
String b="b";
String c= "c";
void setup()
{
    t1=millis();
    pinMode(7, OUTPUT);
    mySerial.begin(9600);
    pump.begin(9600);
    presion.begin(9600);
    digitalWrite(7,HIGH);
//    prueba= "prueba";
}
void medir(){
  
  if((t2-t1)>= 2000){
  //mySerial.println("measure");
  /*SONDA*/
  sonda=analogRead(6);
  sonda=sonda * (5.0 / 1023.0 );
  mySerial.print("c");
  mySerial.println(sonda);
  delay(80);
    if (seleccion == 1){
      //mySerial.println("presion:");
      delay(50);
      conversion(respuesta,1);
      pump.listen();
      digitalWrite(7,HIGH);
      pump.println("#007STA");
      delay(80);
      seleccion = 2;
    }
    else{
      //mySerial.println("presion:");
      delay(50);
      conversion(respuesta_pump,2);
      presion.listen();
      //mySerial.print(prueba[1]+prueba[2]+prueba[3]);
      digitalWrite(7,LOW);
      presion.println("@253PR1?;FF");
      delay(80);
      seleccion=1; 
    }
  t1=t2;
  }
}
void conversion(char respuestas[],int equipo_){
  //String pump_state(respuestas[1]);
  if (equipo_ == 1){
//  String d1(respuestas[8]);
//  String d2(respuestas[10]);
//  String d3(respuestas[11]);
//  String d5(respuestas[13]);
//  String d4(respuestas[14]);
//  dt1 = (b+d1+d2+d3+d5+d4);
  //mySerial.println("preasured");
  mySerial.print("a");
  mySerial.print(respuestas[8]);
  mySerial.print(respuestas[10]);
  mySerial.print(respuestas[11]);
  mySerial.print(respuestas[13]);
  mySerial.print(respuestas[14]);
  mySerial.println();
  }
  if (equipo_ == 2){
    //String d1(respuestas[8]);
    //if (d1=="1"){ //Verifica que la bomba este encendida
//      String d2(respuestas[9]);//on
//      String d3(respuestas[10]);//speed reached
//      String d5(respuestas[11]);//standby
//      String d4(respuestas[27]);//respuesta velocidad
//      String d6(respuestas[28]);
//      String d7(respuestas[29]);
//      String d8(respuestas[30]);
//      String d9(respuestas[31]);
      //String d10(rand());
      //dt2 = (a+d4+d6+d7+d8+d9);
    //}
//    else{
//      dt2="b00000";
//    }
    //mySerial.println("pumped");
    mySerial.print("b");
    for (int x=27; x< 32; x++){
      if (respuestas[x] == ','){
        break;
      }
      mySerial.print(respuestas[x]);
    }
    mySerial.println();

    //mySerial.println(dt2); 
  }
  delay(10); 
}
void loop()
{   
  int i=1;
  
/*CODIGO*/
  while(pump.available()>0) {
     char ch = pump.read();
     if (ch != ' '){
      respuesta_pump[i]=ch;
      i = i+1; 
      if (i == 33){
        break;
      }
     }
     //mySerial.print(respuesta_pump[i]);
     delay(10); 
     
  }
  i=1;
/*Lectura presion*/
    while(presion.available()>0) {
     char ch = presion.read();
     if (ch != ' '){
      respuesta[i]=ch;
      //mySerial.print(respuesta[i]);
     }
     delay(10);
     i = i+1;  
  }
  i=0;
  t2=millis();
  medir(); 
  if (i2c % 2 ==1){
    dtsend=dt1;
  }
  else{
    dtsend=dt2;
  } 
}
