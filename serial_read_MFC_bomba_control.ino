#include <SoftwareSerial.h>
#include <stdio.h>
#include <stdlib.h>
SoftwareSerial mySerial(8, 9);
SoftwareSerial esp(10,11);
SoftwareSerial pump(2,3); 
char* presion;
char bomba;
char flujo;
unsigned long t1=0;
unsigned long t2=0;
bool turbo=false;
int var=1;
//Identifica el dispositivo del que se lee
//Presion =1, Bomba =2, MKS =3;
int equipo;
//int d2;
void setup() {
  Serial.begin(9600);  // put your setup code here, to run once:
  mySerial.begin(9600);
  pump.begin(9600);
  esp.begin(9600);
  /*Bomba turbo*/
  /*ASIGNAR DIRECCION*/ 
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("A");
//  Serial.print("D");
//  Serial.print("R");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.println("7");
/*DEFINIR OPCION*/

/*ENCENDER-APAGAR BOMBA TURBO*/
//#adrTMPON<CR>
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("7");
//  Serial.print("T");
//  Serial.print("M");
//  Serial.print("P");
//  Serial.print("O");
//  Serial.println("N");
//#adrTMPOFF<CR>
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("7");
//  Serial.print("T");
//  Serial.print("M");
//  Serial.print("P");
//  Serial.print("O");
//  Serial.print("F");
//  Serial.println("F");

/*STATUS*/
//#adrSTA<CR>[<LF>]
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("7");
//  Serial.print("S");
//  Serial.print("T");
//  Serial.println("A");
 /*---------MKS-----------*/ 
 /*ENCIENDA REMOTE MODE */
//  Serial.print("R");
//  Serial.print("T");
//  Serial.print(",");
//  Serial.print("O");  
//  Serial.println("N");
//  delay(50);
//  Serial.print("?");
//  Serial.print("R");
//  Serial.println("T");
///*ASK FOR CHANNEL*/
//  //mySerial.println("Data from MKS");
//  delay(50);
//  Serial.print("?");
//  Serial.print("P");
//  Serial.println("Y");
  delay(50);
//  Serial.print("A");
//  Serial.print("V");
//  Serial.println("1"); 
//  delay(50);
//  Serial.print("A");
//  Serial.print("V");
//  Serial.println("2"); 

/*MEDIDOR PRESION*/
  //Serial.println("@253ADC?;FF");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(20);
  //Serial.println("@253PR1?;FF");
  delay(100);
  //MedirTurbo();
  mySerial.println("txt>");
  equipo=1;
  MedirPresion();
}
/*Funcion Presion*/
void MedirPresion(){
  delay(20);
  esp.println("@253PR1?;FF");
  //t1= millis();
}
/*Funcion Velocidad de la Bomba Turbo*/
void MedirTurbo(){
  delay(20);
  //if (turbo==true){
  pump.println("#007STA");
  //pump.listen();
  //  }
  //t1= millis();
    
}
void loop() { 
  //Variable para guardar el mensaje recibido
  char respuesta[50];
  char respuesta_pump[80];
  char respuesta_presion[50];
  
  int i = 1;
  int j=1;
  bool enviado =false;
  bool flag_presion=true;// por ahora ->testing
  while(Serial.available()>0) {
     char ch = Serial.read();
     if (ch != ' '){
      respuesta[i]=ch;
     }
     //mySerial.println(ch);
     //mySerial.println("recibido>");
     delay(10);
     //mySerial.write(ch);
     delay(1); 
     i = i+1;  
     enviado = true;
  }
    //i=1;
    while (pump.available()>0){
     char ch_pump = pump.read();
     if (ch_pump != ' '){
      respuesta_pump[j]=ch_pump;
     }
     //delay(10);
     //mySerial.println("pump");
     //mySerial.println(ch_pump); 
     j = j+1;
     enviado = true; 
    equipo = 1;
    }

  //--------------------------
  if (enviado == true){
    //mySerial.print("R:");
    if (equipo == 1){
      //for (int x; x<=i; x++){
      //mySerial.print(respuesta[x]);
      //}
      enviado=false;
      i=0;
      //mySerial.println(respuesta[8]);
      String d1(respuesta[8]);
      String d2(respuesta[10]);
      String d3(respuesta[11]);
      //Serial.println(presion);
      //mySerial.println(respuesta[10]);
      //mySerial.println(respuesta[11]);
      //if (respuesta[13]=='+'){
          String d5(respuesta[13]);
          String d4(respuesta[14]);
          //mySerial.println(d1+d2+d3+d4);
          String dt = (d1+d2+d3+d5+d4);
          mySerial.println(dt);
          delay(20);
          //envio medicion
          esp.println(dt);
          equipo=2;
    }
    if(equipo == 2){
//      for (int x; x<=j; x++){
//        mySerial.print(respuesta_pump[x]);
//        
//      }
      enviado=false;
      j=0;
      //mySerial.println("eq2:");
      //mySerial.println(respuesta[8]);
      String d1(respuesta_pump[27]);
      String d2(respuesta_pump[28]);
      String d3(respuesta_pump[29]);
      //Serial.println(presion);
      //mySerial.println(respuesta[10]);
      //mySerial.println(respuesta[11]);
      //if (respuesta[13]=='+'){
      String d5(respuesta_pump[30]);
      String d4(respuesta_pump[31]);
      //mySerial.println(d1+d2+d3+d4);
      String dt = (d1+d2+d3+d5+d4);
      //mySerial.println("pump");
      delay(100);
      mySerial.println(dt);
      delay(20);
      //envio medicion
      esp.println(dt);
     //esp.listen();
     equipo=1;
      
    }
  }
    
  if ((t2-t1) >= 1000){//MEDIR CADA 2.5 SEGUNDOS INTERCALADO PARA NO SATURAR LA COMUNICACION
      var=var+1;
      t1=t2;
      }
      
//----MEDICION----//
    if (var == 1){
      MedirPresion();
      delay(200);}
    if (var==2){
      MedirTurbo();
      delay(200);
      var=0;}
          
    delay(10);
    //esp.listen();
    while (esp.available()>0){
      char tbp=esp.read();
     if (tbp != ' '){
      respuesta_presion[j]=tbp;
      //mySerial.print(ch_pump);
     }
     //delay(10);
     //mySerial.println("pump");
     //mySerial.println(ch_pump); 
     j = j+1;
     enviado = true; 
    equipo = 2;
    }
   
    t2=millis();
}
