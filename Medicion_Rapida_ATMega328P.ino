#include <Filters.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
float medicion=0;
float medicion_fuente=0;
float medicion_buffer[50];
float medicion_buffer_fuente[50];
float med_ant=0;
float med_fuente_ant=0;
float max_fuente=0;//Encuentra el valor pico
float A,B=0;
float pendiente=0;
float pendiente_prom=0;
float max_pendiente=0;
int conta =1;
int contb =0;
unsigned long t1=0;
unsigned long t2=0;
unsigned long tiempo=0;
unsigned long tiempoX=0;
bool pico =false;
bool periodo_fin=false;
float medX=0;
float medY=0;
float medXY=0;
float medX2 =0;
FilterOnePole lowpassFilter( LOWPASS, 5 );
FilterOnePole lowpassFilter2( LOWPASS, 5 );
/*Maxima corriente medida con Multimetro Agilent 34405a
 * Gas:Nitrogeno
 * A 100V+ I=0.17 A max
 * Para un plasma Glow discharge
 * Presion:6.95-2
 * Flujo:5 SCCM
 * Fuente: Voltaje= 500V  Corriente= 20 mA
 * Max medicion de corriente registrada traducida a voltaje:2.92
 */
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  t1 =millis();
  Serial.println("--Sonda start--");
  pinMode(13, OUTPUT);
  digitalWrite(13,HIGH);
}

void loop() {
  //medicion=analogRead(A0);
  //medicion=medicion_buffer[cont] * (5.0 / 1023.0 );
 // medicion=(medicion_buffer[1]+medicion_buffer[2]+medicion_buffer[3]+medicion_buffer[4]+medicion_buffer[5]+medicion_buffer[6]+medicion_buffer[7]+medicion_buffer[8]+medicion_buffer[9]+medicion_buffer[10])/10
  medicion=lowpassFilter.input( analogRead( A1 ) );
  medicion=medicion * (5.0 / 1023.0 );
  /*Filtro media movil*/
  /*if(conta < 5){
    medicion_buffer[5 - cont]=medicion;
    conta++;
  }
  else{
    medicion_buffer[5]=medicion_buffer[4];
    medicion_buffer[4]=medicion_buffer[3];
    medicion_buffer[3]=medicion_buffer[2];
    medicion_buffer[2]=medicion_buffer[1];
    medicion_buffer[5]=medicion;
  }
  medicion=(medicion_buffer[1]+medicion_buffer[2]+medicion_buffer[3]+medicion_buffer[4]+medicion_buffer[5])/5;
  *//*Medicion Fuente*/
  medicion_fuente=analogRead( A0 );//lowpassFilter2.input( analogRead( A0 ) );
  medicion_fuente=medicion_fuente * (5.0 / 1023.0 );
  /*Revision maximo*/
  if (medicion_fuente > max_fuente){ 
    max_fuente=medicion_fuente; 
  }
  /*Revision de flanco*/
  if (medicion_fuente >= 0.9*max_fuente){
    pico =true;
  }
  if(medicion_fuente < 0.4*max_fuente && pico == true){
    //mide tiempo
    t2=millis();
    tiempo=t2-t1;
//    Serial.print(tiempo);
//    Serial.print(",");
    t1=t2;
    pico =false; 
    periodo_fin=true;
    
  }
    tiempo=3000;//FIXME
    t2=millis();
    tiempoX=t2-t1;
    if(tiempoX >= tiempo){//FIXME>quick workaround para picos
      t1=t2;
      pico =false; 
      periodo_fin=true;
    }
    
    A=(120000 / tiempo);//100/tiempo
    B=-20;//min_fuente;//-20
    medicion_fuente=(A*(tiempoX)/1000)-20;
  /*Filtro media movil*/
    /*if(contb < 5){
    medicion_buffer_fuente[5 - cont]=medicion_fuente;
    contb++;
  }
  else{
    medicion_buffer_fuente[5]=medicion_buffer_fuente[4];
    medicion_buffer_fuente[4]=medicion_buffer_fuente[3];
    medicion_buffer_fuente[3]=medicion_buffer_fuente[2];
    medicion_buffer_fuente[2]=medicion_buffer_fuente[1];
    medicion_buffer_fuente[5]=medicion_fuente;
  }
  medicion_fuente=(medicion_buffer_fuente[1]+medicion_buffer_fuente[2]+medicion_buffer_fuente[3]+medicion_buffer_fuente[4]+medicion_buffer_fuente[5])/5;
  */
  //Serial.print(tiempoX);
  //Serial.print(",");
  medicion=medicion-1.1;
  /*convrsion a mA*/
  medicion=58.22*medicion;
/*Obtencion de pendiente y parametros del plasma
 * Aproximacion de pendiente por minimos cuadrados
*/if ((tiempoX >= (tiempo*0.5)) && (tiempoX <= (tiempo*0.55))){
    medicion_buffer[conta]=medicion; //Y
    medicion_buffer_fuente[conta]=medicion_fuente;//X
    conta++;
}
/*cuando termina el periodo calcula*/
  if (periodo_fin == true){
    //Suma X
    for (int x=1; x <= conta; x++){
      medX=medX + medicion_buffer_fuente[x];
      medY=medY + medicion_buffer[x];
      medXY= medXY +(medX*medY);
      medX2=medX2 +(pow(medX,2));
    }
    /*Calculo de la temperatura (pendiente)*/
    pendiente=(medXY-((medX*medY)/conta)) / (medX2-(pow(medX,2)/conta));
    conta=1;
    medX=0;
    medY=0;
    medXY=0;
    medX2=0;
    periodo_fin=false;
     
    /*Grafica*/
    Serial.print(medicion);
    Serial.print(",");
    Serial.print(medicion_fuente);
    Serial.print(",");
    Serial.println((pendiente));
  }

  

  med_ant=medicion;
  med_fuente_ant=medicion_fuente;
  // put your main code here, to run repeatedly:

}
