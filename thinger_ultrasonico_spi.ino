#include <ESP8266WiFi.h>
#include <ThingerESP8266.h>
#include <X9C.h>
#include <stdio.h>
#include <stdlib.h>
#include <SoftwareSerial.h>

/*Credenciales Thinger.io*/
#define USERNAME "EduardoBadilla"
#define DEVICE_ID "ESP8266"
#define DEVICE_CREDENTIAL "1OQl9FG@o%Ja"

#define SSID "AndroidAP"//"belkin.3433"
#define SSID_PASSWORD "rzzf9130"//"2433966a"

/*Potenciometro Digital*/
#define INC D5//2   // D1 Mini D4 - pulled up in H/W (10k) ->  chip pin 1
#define UD D6//15   // D1 Mini D8                          ->  chip pin 2
#define CS D7//16 
ThingerESP8266 thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);
SoftwareSerial Arduino(D3,D4);
X9C pot;
String mks;
char buff[10];
char respuesta[50];
char respuesta_mks[30];
bool activado=false;
bool up_conversion=false;
int i=1;
int indice;
int resistencia=0;
String dt;
String mks_flujo;
long presion;
volatile float presion_ant;
int voltaje_sonda;
int vel_turbo;
int intentos=5;
unsigned long t1=0;
unsigned long t2=0;
long presiontot;
//CONTROL DEL RECIBIDO DE MENSAJES
//Presion=1, Bomba=2, MKS=3;
int equipo=0;





void conversion1(){
  String d1(respuesta[1]);
  String d2(respuesta[2]);
  String d3(respuesta[3]);
  String d4(respuesta[4]);
  String d5(respuesta[5]);
  String d6(respuesta[6]);
  delay(20);
  if (d1=="a"){
    dt=(d2+d3+d4);
    presion=dt.toInt();
    indice= d6.toInt();
    d5="+";
    if (d5=="+"){
      presiontot=(presion);//*1000)*(pow(10,indice));//1000
    }
    else{
      presiontot=(presion*1)/(pow(10,indice));//1000
    }
    if( presion_ant*0.01 >= presiontot ){
      presiontot=presion_ant;
    }
    presion_ant=presiontot;
  }
  if (d1=="b"){
    dt=(d2+d3+d4+d5+d6);

    vel_turbo=dt.toInt();
  }
  if(d1=="c"){
    dt=(d2+d4+d5);
    voltaje_sonda=dt.toInt();
    }
}
void conversion2(){
  String d1mks(respuesta_mks[1]);
  String d2mks(respuesta_mks[2]);
  String d3mks(respuesta_mks[3]);
  delay(20);
  mks_flujo=(d1mks+d2mks+d3mks);
  Serial.println(mks_flujo);
}
void setup() {
  t1=millis();
  Serial.begin(9600);
  Arduino.begin(9600);
  pot.begin(CS,INC,UD);
  //Wire.begin(D1, D2);
  
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  pinMode(13, OUTPUT);//trigger
  digitalWrite(13,LOW);
  
  //THINGER.IO
  thing.add_wifi(SSID, SSID_PASSWORD);
  thing["Flujo"] >> [] (pson& out){
    conversion2();
    out=mks_flujo;
  };
  // digital pin control example (i.e. turning on/off a light, a relay, configuring a parameter, etc)
  thing["led"] << digitalPin(LED_BUILTIN);
  // resource output example (i.e. reading a sensor value)
  
  thing["Voltaje Fuente"] << inputValue(resistencia,{
    pot.setPot(resistencia,true);
    delay(10);
  });
    thing["Camara"]>> [](pson& out){
      out["Flujo"]=mks_flujo;
      out["Presion"]=presiontot;
      out["Velocidad"]=vel_turbo;
      out["Tiempo ON"]=millis();
      out["Sonda"]=voltaje_sonda;
    };
  // more details at http://docs.thinger.io/arduino/
}
void MKS(){
  //funcion para medir flujo
  Serial.println("?RT");//Estado de la interfaz remota
  //Requiere encenderse
}
void salidas(){
  
  
  if((t2-t1) >= 900){
    t1=t2;
    MKS();
    delay(50);
    if(digitalRead(LED_BUILTIN)==1){
        Arduino.println("nnn");  /* envio*/
    }
    if(digitalRead(LED_BUILTIN)==0){
        Arduino.println("fff");  /* envio*/
    }
  }
  
}

void loop() {
  /*Leer los datos del microcontrolador*/
  while(Arduino.available()){
    char c = Arduino.read();
    //Serial.print(c);
    if (c != ' '){
        respuesta[i]=c;
        i = i+1;
    }
    up_conversion=true;
  }
  i=1;
  if (up_conversion==true){
    conversion1();
    up_conversion=false;    
  }
  ///----------------------------------------------
    while(Serial.available()>0) {
       char ch1 = Serial.read();
       
       if (ch1 != ' '){
        //Serial.println(ch1);
        respuesta_mks[i]=ch1;
        i = i+1;
       }
       delay(10);
    }
   i=1;

  ///------------------------------------------
  
  
  delay(20);
  //Serial.println(dt);
  thing.handle();
  t2=millis();
  salidas();
  
}
