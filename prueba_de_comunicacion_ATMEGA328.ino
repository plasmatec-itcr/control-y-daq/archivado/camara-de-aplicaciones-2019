#include <SoftwareSerial.h>
#include <Filters.h>
#include <stdio.h>
#include <stdlib.h> 
//SoftwareSerial Serial(5,4);  //rx, tx
SoftwareSerial esp(7,8);
SoftwareSerial presion(4,6);
FilterOnePole lowpassFilter( LOWPASS, 5 );
FilterOnePole lowpassFilter2( LOWPASS, 5 );
//RunningStatistics inputStats;
//float windowLength =10;
//inputStats.setWindowSecs( windowLength ); 
/*Variables*/
/*VARIABLES GLOBALES*/
static int led_funcionamiento=13;
static int instruccion_bomba=2;
int chn=0; 
int i2c;
/*Variables sonda*/
int conta=1;
int contb=1;
bool periodo_fin=false;
bool estado_actual;
bool estado_anterior;
unsigned long tiempo=1;
unsigned long tiempoX=0;
float medicion_buffer[50];
float medicion_buffer_fuente[50];
float pendiente=0;
float pendiente_media=0;
float pendiente_mediaf=0;
float max_fuente;//Encuentra el valor pico
float medicion=0;
float medicion_fuente=0;
float A,B=0;
float pendiente_prom=0;
float max_pendiente=0;
bool pico =false;
float medX=0;
float medY=0;
float medXY=0;
float medX2=0;
float Num=1;
float Den=1;
/*--------*/
bool orden_flagON;
bool orden_flagOFF;
bool enviado=false;
unsigned long t1=0;
unsigned long t2=0;
unsigned long tf1=0;
unsigned long tf2=0;
/*Bomba*/
String dt_bomba;
bool estado_bomba;
bool estado_instruccion;
bool estado_instruccion_ant;
int seleccion=0;
volatile char respuesta_pump[50];
volatile char respuesta[20];
volatile char orden_esp;
char prueba[10];
String dt2;
String dt1;
String dtsend;
/*Equipo*/
String a="a";
String b="b";
String c= "c";
/*Tiempo de solicitudes
default:2000*/
//int tiempo_respuesta = 2000; 
int solicitud=0;
int cont_respuestas=0;
int conversiones=0;
bool testing =false;
int cont_test=0;

/**SETUP**/
void setup()
{
    t1=millis();
    tf1=t1;
    pinMode(led_funcionamiento, OUTPUT);
    pinMode(instruccion_bomba, INPUT);
    pinMode(4, INPUT);
    pinMode(6, OUTPUT);
    pinMode(7, INPUT);
    pinMode(8, OUTPUT);
    Serial.begin(9600);
    esp.begin(9600);
    presion.begin(9600);
    digitalWrite(led_funcionamiento,HIGH);
    esp.println("--test ATMega328--");
}

/*TEMPERATURA*/
void temperatura(){
  medicion=lowpassFilter.input( analogRead( A1 ) );
  medicion=medicion * (5.0 / 1023.0 );
  medicion_fuente=analogRead( A0 );//lowpassFilter2.input( analogRead( A0 ) );
  medicion_fuente=medicion_fuente * (5.0 / 1023.0 );
    /*Revision maximo*/
  estado_actual=digitalRead(A0);
  if((estado_actual != estado_anterior)&&(estado_actual == LOW)){
    //mide tiempo
    tf2=millis();
    tiempo=tf2-tf1;
//    Serial.print(tiempo);
//    Serial.print(",");
    tf1=tf2;
    pico =false; 
    periodo_fin=true;
    
  }
  estado_anterior=estado_actual;
    //tiempo=3000;//FIXME
    
    tiempoX=tf2-tf1;
    //Serial.println(tiempoX);
//    if(tiempoX >= tiempo){//FIXME>quick workaround para picos
//      tf1=tf2;
//      pico =false; 
//      periodo_fin=true;
//    }
    
    A=(120000 / tiempo);//100/tiempo
    B=-20;//min_fuente;//-20
    medicion_fuente=(A*(tiempoX)/1000)-20;
    medicion=medicion-1.1;
    /*convrsion a mA*/
    medicion=58.22*medicion;

    //if ((tiempoX >= (tiempo*0.48)) && (tiempoX <= (tiempo*0.55))){//fixme 
      /*prueba con medicion de corriente en vez de margen de tiempo*/
    if((medicion >= 50) && (medicion < 150)){
      if((tiempoX >= (tiempo*0.3)) && (tiempoX <= (tiempo*0.8))){
      medicion_buffer[conta]=medicion; //Y
      medicion_buffer_fuente[conta]=medicion_fuente;//X
      delay(20);
      conta=conta+1;
      }
    }
    /*Calculos*/
    if (periodo_fin == true){
    //Suma X
      for (int x=1; x < conta; x++){
        medX=medX + medicion_buffer_fuente[x];
        medY=medY + medicion_buffer[x];
        medXY= medXY +(medX*medY);
        medX2=medX2 +(pow(medX,2));
      }
      /*Calculo de la temperatura (pendiente)*/
      int n=conta-1;
      Num=medXY-((medX*medY)/n);
      Den=medX2-(pow(medX,2)/n);
      float pendiente_ant=pendiente;
      pendiente=Num/Den;//(medXY-((medX*medY)/conta)) / (medX2-(pow(medX,2)/conta));
      if(isnan(pendiente)){
        pendiente=pendiente_ant;
      }
//      pendiente_media=pendiente_media+pendiente;
//      if (contb == 10){
//        pendiente_mediaf=pendiente_media/10;
//        contb=1;
//        pendiente_media=0;
//      }
//      contb++;
//      delay(25);
      medX=0;
      medY=0;
      medXY=0;
      medX2=0;
      periodo_fin=false;
      conta=1;
      }
}
/*MEDIR */
void medir(){
  /*Calcular valores sonda*/
  if((t2-t1)>= 2000){ /*Definir la frecuencia de solicitudes de respuesta*/
  //FIXME temporal

//  if(estado_bomba==true){
//    //Si la instruccion esta encendida y esta apagada, encienda aunque la velocidad no sea cero
//    //Avise del estado>digital pin
//    dt_bomba="1";
//  }
//  else{
//    dt_bomba="0";
//  }
  if(testing == false){
    cont_test++;
  }
  if (cont_test == 5){//espera 10 segundos para iniciar
    testing = true;
    esp.println("--test--");
    esp.println("p");
    //delay(30);
    cont_test++;
  }
  /*SONDA*/
  //sonda=analogRead(6);
  //sonda=sonda * (5.0 / 1023.0 );
  esp.print("c");
  esp.println(pendiente);
  delay(80);
    if (seleccion == 1){
      //Serial.println("presion:");
      //delay(50);
      conversion(respuesta,1);
      //pump.listen();
      digitalWrite(led_funcionamiento,HIGH);
       
      /*Revise el estado del pin de instruccion, actual y anterior*/
      estado_instruccion=digitalRead(instruccion_bomba);
      if((estado_instruccion == HIGH)&&(estado_instruccion_ant==LOW)){
      //if(orden_flagON == false && orden_flagOFF ==false){
        //pump.println("#007STA");
        Serial.println("#007TMPON");
        delay(20);
      }
      //else if (orden_flagON == true){
      else if((estado_instruccion == LOW)&& (estado_instruccion_ant == HIGH)){
        Serial.println("#007TMPOFF");
        delay(20);
        //pump.println("#007TMPON");
        //orden_flagON=false;
      }
      else{
        Serial.println("#007STA");
//      else if (orden_flagOFF == true){
//        pump.println("#007TMPOFF");
//        orden_flagOFF=false;
      }
      estado_instruccion_ant = estado_instruccion;
      //delay(80);
      seleccion = 2;
      if(testing == true)solicitud++;
      //Serial.print("solicitudes: ");
      //Serial.println(solicitud);
    }
    else{
      //Serial.println("presion:");
      //delay(50);
      conversion(respuesta_pump,2);
      presion.listen();
      //Serial.print(prueba[1]+prueba[2]+prueba[3]);
      digitalWrite(led_funcionamiento,LOW);
      presion.println("@253PR1?;FF");
      //delay(80);
      seleccion=1;
      if(testing == true)solicitud++;
      //Serial.print("solicitudes: ");
      //Serial.println(solicitud); 
    }
  t1=t2;
  }
}
void conversion(char respuestas[],int equipo_){
  esp.listen();
  //String pump_state(respuestas[1]);
  if (equipo_ == 1){
  String d1(respuestas[8]);
  String d2(respuestas[10]);
  String d3(respuestas[11]);
  String d5(respuestas[13]);
  String d4(respuestas[14]);
  dt1 = (a+d1+d2+d3+d5+d4);
  //Serial.println("preasured");
  //Serial.print("a");
  //TODO: revisar respuesta
  if(respuestas[8] != ' ' || respuestas[8] != '0'){
    if(testing == true)conversiones++;
    //Serial.println("conversiones");
    //Serial.println(conversiones);
  }
//  Serial.print(respuestas[8]);
//  Serial.print(respuestas[10]);
//  Serial.print(respuestas[11]);
//  Serial.print(respuestas[13]);
//  Serial.print(respuestas[14]);
  esp.println(dt1);
  }
  if (equipo_ == 2){
    //String d1(respuestas[8]);
    //if (d1=="1"){ //Verifica que la bomba este encendida
//      String d2(respuestas[9]);//on
//      String d3(respuestas[10]);//speed reached
//      String d5(respuestas[11]);//standby
//      String d4(respuestas[27]);//respuesta velocidad
//      String d6(respuestas[28]);
//      String d7(respuestas[29]);
//      String d8(respuestas[30]);
//      String d9(respuestas[31]);
      //String d10(rand());
      //dt2 = (a+d4+d6+d7+d8+d9);
    //}
//    else{
//      dt2="b00000";
//    }
    //Serial.println("pumped");
//    Serial.print(respuestas[1]);
//    Serial.print(respuestas[2]);
//    Serial.print(respuestas[3]);
//    Serial.print(respuestas[4]);
//    Serial.print(respuestas[5]);
//    Serial.print(respuestas[6]);
//    Serial.print(respuestas[7]);
//    Serial.println(respuestas[8]);
//    delay(100);
      if(respuesta_pump[8] == '1'){//FIXME no lee adecuadamente la respuesta
        //del equipo 
      dt_bomba="1";
      }
      else{
      dt_bomba="0";
      }
    for(int y=1; y< 10;y++){
      esp.print(respuestas[y]);
    }
    delay(20);
    esp.print("b");
    for (int x=27; x< 32; x++){
//      if (respuestas[x] == ','){
//        break;
  //      }
      esp.print(respuestas[x]);
      if(respuestas[8] != ' ' || respuestas[8] != '0'){
        if(testing == true)conversiones++;
        //Serial.println("conversiones");
        //Serial.println(conversiones);
      }
    }
    esp.print(",");
    esp.print(dt_bomba);
    esp.println();

    //Serial.println(dt2); 
  }
  delay(10); 
}
void loop()
{   
  int i=1;

/*CODIGO*/
/*Se utiliza puerto serial para la bomba debido a la extension de su respuesta*/
  while(Serial.available()>0) {
     char ch = Serial.read();
     if (ch != ' '){
      respuesta_pump[i]=ch;
      //Serial.print(respuesta_pump[i]);
      /*Habilitar senal de encendido*/

      i = i+1; 
      if (i == 33){
        if((testing == true) && (respuesta_pump[1] == '#'))cont_respuestas++;
        //Serial.print("respuestas");
        //Serial.print(respuestas);
        //break;
      }
     }
     //Serial.print(ch);
     delay(10); 
  }
  i=1;
/*Lectura presion*/
    while(presion.available()>0) {
     char ch = presion.read();
     if (ch != ' '){
      respuesta[i]=ch;
      if (i == 14){
        if((testing == true) && (respuesta[1]== '@'))cont_respuestas++; 
      }
      //Serial.print(respuesta[i]); TODO: revisar la respuesta y ponerle print
     }
     delay(10);
     i = i+1;  
  }
  i=1;
  
///*Pump reacomodo de puertos*/
//  while (Serial.available()>0){
//    char ch = Serial.read();
//    Serial.print(ch);
//    delay(1);
//    if (ch != ' '){
//      orden_esp=ch;
//      if(orden_esp == 'd'){
//        //send flag a medir
//        orden_flagON=true;
//        orden_esp='_';
//      }
//      else if (orden_esp == 'e'){
//        orden_flagOFF=true;
//        orden_esp='_';
//      }
//    }
//  }
  temperatura();  
  t2=millis();
  tf2=millis();
  medir(); 
  if (i2c % 2 ==1){
    dtsend=dt1;
  }
  else{
    dtsend=dt2;
  }
  if (cont_respuestas >= 100){
     Serial.println("------------");
     Serial.print("S:");
     Serial.println(solicitud);
     Serial.print("R:");
     Serial.println(cont_respuestas);
     Serial.print("C:");
     Serial.println(conversiones);
     solicitud= 0;
     cont_respuestas= 0;
     conversiones = 0;
     cont_test=0;
     testing = false;
     for (int x=0; x <3; x++){
      Serial.println("z");
      delay(80);
     }
  }
}
