#include <SoftwareSerial.h>
#include <Wire.h>
#include <stdio.h>
#include <stdlib.h>
SoftwareSerial mySerial(8, 9);
SoftwareSerial presion(10,11);
SoftwareSerial pump(2,3); 
char bomba;
char flujo;
unsigned long t1=0;
unsigned long t2=0;
bool turbo=false;
bool enviado =false;
int var=1;
String dt1;
String dt2;
String dt3;
String cero="0";
String a="a";
String b="b";
String c="c";
String dtsend;
//Identifica el dispositivo del que se lee
//Presion =1, Bomba =2, MKS =3;
int equipo;
int i2c;
//int d2;
void setup() {
  Wire.begin(8);                /* join i2c bus with address 8 */
  Wire.onReceive(receiveEvent); /* register receive event */
  Wire.onRequest(requestEvent);
  Serial.begin(9600,SERIAL_7O1);  // put your setup code here, to run once:
  mySerial.begin(9600);
  pump.begin(9600);
  presion.begin(9600);
  /*Bomba turbo*/
  /*ASIGNAR DIRECCION*/ 
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("A");
//  Serial.print("D");
//  Serial.print("R");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.println("7");
/*DEFINIR OPCION*/

/*ENCENDER-APAGAR BOMBA TURBO*/
//#adrTMPON<CR>
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("7");
//  Serial.print("T");
//  Serial.print("M");
//  Serial.print("P");
//  Serial.print("O");
//  Serial.println("N");
//#adrTMPOFF<CR>
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("7");
//  Serial.print("T");
//  Serial.print("M");
//  Serial.print("P");
//  Serial.print("O");
//  Serial.print("F");
//  Serial.println("F");

/*STATUS*/
//#adrSTA<CR>[<LF>]
//  Serial.print("#");
//  Serial.print("0");
//  Serial.print("0");
//  Serial.print("7");
//  Serial.print("S");
//  Serial.print("T");
//  Serial.println("A");
 /*---------MKS-----------*/ 
 /*ENCIENDA REMOTE MODE */
//  Serial.print("R");
//  Serial.print("T");
//  Serial.print(",");
//  Serial.print("O");  
//  Serial.println("N");
//  delay(50);
//  Serial.print("?");
//  Serial.print("R");
//  Serial.println("T");
///*ASK FOR CHANNEL*/
//  //mySerial.println("Data from MKS");
//  delay(50);
//  Serial.print("?");
//  Serial.print("P");
//  Serial.println("Y");
  delay(50);
//  Serial.print("A");
//  Serial.print("V");
//  Serial.println("1"); 
//  delay(50);
//  Serial.print("A");
//  Serial.print("V");
//  Serial.println("2"); 

/*MEDIDOR PRESION*/
  //Serial.println("@253ADC?;FF");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(20);
  //Serial.println("@253PR1?;FF");
  delay(100);
  //MedirTurbo();
  mySerial.println("txt>");
  //equipo=1;
  //MedirPresion();
}
void checkeo(){
  /// IMPLEMENTAR VERIFICACION DE ENVIOS DE COMANDOS
}

void receiveEvent(int howMany) {
 while (0 <Wire.available()) {
    char tbp = Wire.read();      /* receive byte as a character */
    //Serial.print(tbp);           /* print the character */
      if (tbp == 'n'){
        pump.println("#007TMPON");
        pump.println("#007TMPON");
        pump.println("#007TMPON");
        turbo=true;
      }
      //if (tbp == 'f' && turbo==true){
      if (tbp == 'f'){
        pump.println("#007TMPOFF");
        pump.println("#007TMPOFF");
        pump.println("#007TMPOFF");
        //turbo==false;
      }
      if (tbp == 'x' && turbo==true){
        digitalWrite(13,HIGH);
      }
      if (tbp == 'y'){//APAGAR EN CUALQUIER MOMENTO
        digitalWrite(13,LOW);
      }
  }
 //Serial.println();             /* to newline */
}

void requestEvent() {
 //if(enviado==true){
 i2c=i2c+1;
 Wire.println(dtsend);
 //Wire.print(dt2);
 

 //}

 /*send string on request */
}
/*Funcion Presion*/
void MedirPresion(){
  delay(20);
  presion.println("@253PR1?;FF");
  //t1= millis();   
}
/*Funcion Velocidad de la Bomba Turbo*/
void MedirTurbo(){
  delay(20);
  //if (turbo==true){
  pump.println("#007STA");
    //  }
  //t1= millis();
    
}
void Medirflujo(){///------------------------SIRVE
  Serial.println("?RT");
  delay(20);
  mySerial.println("RT>");
  //pump.println("#007STA");
}
void loop() { 
  //Variable para guardar el mensaje recibido
  char respuesta[50];
  char respuesta_pump[80];
  char respuesta_mks[50];
  
  int i = 1;
  int j=1;
  int k=1;

  bool flag_presion=true;// por ahora ->testing
  //presion.listen();
  while(presion.available()>0) {
     char ch = presion.read();
     if (ch != ' '){
      respuesta[i]=ch;
      //Serial.println(ch);
     }
     //mySerial.println(ch);
     //mySerial.println("recibido>");
     delay(10);
     //mySerial.write(ch);
     delay(1); 
     i = i+1;  
     enviado = true;
     equipo=1;
  }
    //i=1;
    //pump.listen();
    while (pump.available()>0){
     char ch_pump = pump.read();
     if (ch_pump != ' '){
      respuesta_pump[j]=ch_pump;
     }
     //delay(10);
     //mySerial.println("pump");
     //mySerial.println(ch_pump); 
     j = j+1;
     enviado = true; 
    equipo = 2;
    }
    
    while (Serial.available()>0){
      char mks=Serial.read();
      mySerial.print(mks);
     if (mks != ' '){
          respuesta_mks[k]=mks;
     }
     k=k+1;
     enviado = true;
     equipo=3;
    }
    
    delay(20);

  //--------------------------
  if (enviado == true){
    //mySerial.print("R:");
    if (equipo == 1){
      //for (int x; x<=i; x++){
      //mySerial.print(respuesta[x]);
      //}
      enviado=false;
      i=0;
      //mySerial.println(respuesta[8]);
      String d1(respuesta[8]);
      String d2(respuesta[10]);
      String d3(respuesta[11]);
      //Serial.println(presion);
      //mySerial.println(respuesta[10]);
      //mySerial.println(respuesta[11]);
      //if (respuesta[13]=='+'){
          String d5(respuesta[13]);
          String d4(respuesta[14]);
          //mySerial.println(d1+d2+d3+d4);
          dt1 = (a+d1+d2+d3+d5+d4);
          mySerial.println("presion");
          mySerial.println(dt1);
          delay(20);
          equipo=2;
    }
    if(equipo == 2){
      for (int x; x<=j; x++){
        Serial.print(respuesta_pump[x]);
        
      }
      delay(100);
      enviado=false;
      j=0;
      //mySerial.println("eq2:");
      //mySerial.println(respuesta[8]);
      String pump_state(respuesta_pump[5]);
      String d1(respuesta_pump[27]);
      String d2(respuesta_pump[28]);
      String d3(respuesta_pump[29]);
      //Serial.println(presion);
      //mySerial.println(respuesta[10]);
      //mySerial.println(respuesta[11]);
      //if (respuesta[13]=='+'){
      String d5(respuesta_pump[30]);
      String d4(respuesta_pump[31]);
      //mySerial.println(d1+d2+d3+d4);
//      if (respuesta_pump[7] == '0'){
//        dt2=cero;
//        }
//      else{
      dt2 = (b+d1+d2+d3+d5+d4);
      //}
      delay(20);
      mySerial.println("turbo");
      mySerial.println(dt2);
      //mySerial.println();
     equipo=3; 
    }
    if (equipo==3){
      k=0;
      String d1(respuesta_mks[1]);
      String d2(respuesta_mks[2]);
      String d3(respuesta_mks[3]);
      String d5(respuesta_mks[4]);
      String d4(respuesta_mks[5]);
      //mySerial.println(d1+d2+d3+d4);
      dt3 = (c+d1+d2+d3+d4+d5);
      mySerial.println("flujo");
      mySerial.println(dt3);
      enviado=false;
      equipo=1;
      delay(20);
      }
  }
  if (i2c % 2 ==1){
    dtsend=dt1;
  }
  else{
    dtsend=dt2;
  }
    
  if ((t2-t1) >= 250){//MEDIR CADA 2.5 SEGUNDOS INTERCALADO PARA NO SATURAR LA COMUNICACION
      t1=t2;
      //----MEDICION----//
    if (var == 1){
      MedirPresion();
      //delay(200);
      }
    if (var==2){
      MedirTurbo();
      //delay(200);
      }
      var=var+1;
     if(var==3){
      Medirflujo();
      //delay(100);
      var=0;
        }
    }
      

          
    delay(10);
    //esp.listen();

//      //if (tbp == 'n' && turbo ==false){
//      if (tbp == 'n'){
//        pump.println("#007TMPON");
//        pump.println("#007TMPON");
//        pump.println("#007TMPON");
//        turbo=true;
//      }
//      //if (tbp == 'f' && turbo==true){
//      if (tbp == 'f'){
//        pump.println("#007TMPOFF");
//        pump.println("#007TMPOFF");
//        pump.println("#007TMPOFF");
//        //turbo==false;
//      }
//      if (tbp == 'x' && turbo==true){
//        digitalWrite(13,HIGH);
//      }
//      if (tbp == 'y'){//APAGAR EN CUALQUIER MOMENTO
//        digitalWrite(13,LOW);
//      }
//      
//    }
   
    t2=millis();
}
