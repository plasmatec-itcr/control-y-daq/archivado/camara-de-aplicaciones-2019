#include <SPI.h>
#include <ESP8266WiFi.h>
#include <ThingerESP8266.h>
#include <stdio.h>
#include <stdlib.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#define USERNAME "EduardoBadilla"
#define DEVICE_ID "ESP8266"
#define DEVICE_CREDENTIAL "bOdcbv2n@4kW"

#define SSID "AndroidAP"//"belkin.3433"
#define SSID_PASSWORD "rzzf9130"//"2433966a"

ThingerESP8266 thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);
SoftwareSerial Arduino(D3,D4);
String mks;
char buff[10];
char respuesta[50];
char respuesta_mks[30];
bool activado=false;
int i=1;
int indice;
String dt;
String mks_flujo;
long presion;
float presion_ant;
int vel_turbo;
int intentos=5;
unsigned long t1=0;
unsigned long t2=0;
long presiontot;
//CONTROL DEL RECIBIDO DE MENSAJES
//Presion=1, Bomba=2, MKS=3;
int equipo=0;





void conversion1(){
  String d1(respuesta[1]);
  String d2(respuesta[2]);
  String d3(respuesta[3]);
  String d4(respuesta[4]);
  String d5(respuesta[5]);
  String d6(respuesta[6]);
  delay(20);
  if (d1=="a"){
    dt=(d2+d3+d4);
    presion=dt.toInt();
    indice= d6.toInt();
    if (d5=="+"){
      presiontot=(presion*1000)*(pow(10,indice));
    }
    else{
      presiontot=(presion*1000)/(pow(10,indice));
    }
    if( presion_ant*0.01 >= presiontot ){
      presiontot=presion_ant;
    }
    presion_ant=presiontot;
  }
  if (d1=="b"){
    dt=(d2+d3+d4+d5+d6);

    vel_turbo=dt.toInt();
  }
}
void conversion2(){
  String d1mks(respuesta_mks[1]);
  String d2mks(respuesta_mks[2]);
  String d3mks(respuesta_mks[3]);
  delay(20);
  mks_flujo=(d1mks+d2mks+d3mks);
}
void setup() {
  t1=millis();
  Serial.begin(9600,SERIAL_7O1);
  Arduino.begin(9600);
  Wire.begin(D1, D2);
  
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  pinMode(13, OUTPUT);//trigger
  digitalWrite(13,LOW);
  
  thing.add_wifi(SSID, SSID_PASSWORD);

  thing["Flujo"] >> [] (pson& out){
    conversion2();
    out=mks_flujo;
  };
  // digital pin control example (i.e. turning on/off a light, a relay, configuring a parameter, etc)
  thing["led"] << digitalPin(LED_BUILTIN);
  
  thing["fuente"] << digitalPin(13);

  // resource output example (i.e. reading a sensor value)
  thing["millis"] >> outputValue(millis());

  thing["Velocidad Bomba Turbo"] >> [] (pson& out){
  conversion1();
  out=vel_turbo;  
  };
  thing["Presion"]>> [] (pson& out){
    conversion1();  
  //i=1;
  //long duration, distance;
  //int i=random(10);
  //char str[12];
  //sprintf(str,"%d",i);
  //itoa(i,j,10);
  //String mensaje=str;
  out=presiontot;
    };
  // more details at http://docs.thinger.io/arduino/
}
void MKS(){
  //funcion para medir flujo
  Serial.println("?RT");//Estado de la interfaz remota
  //Requiere encenderse
}
void salidas(){
  
  
  if((t2-t1) >= 500){
    t1=t2;
    MKS();
    Wire.requestFrom(8,13);
    delay(50);
    if(digitalRead(LED_BUILTIN)==1){
        Wire.beginTransmission(8);
        Wire.write("nnn");  /* envio*/
        Wire.endTransmission();
      //for(int x=0; x<= intentos;x++){Serial.println("ontp");}
    }
    if(digitalRead(LED_BUILTIN)==0){
        Wire.beginTransmission(8);
        Wire.write("fff");  /* envio*/
        Wire.endTransmission();
      //for(int x=0; x<= intentos;x++){Serial.println("oftp");}
    }
    if (digitalRead(13)==1){
      //for(int x=0; x<= intentos;x++){Serial.println("x");}
    }
    if (digitalRead(13)==0){
      //for(int x=0; x<= intentos;x++){Serial.println("y");}
    }
  }
  
}

void loop() {
   /* request & read data of size 13 from slave */
  while(Wire.available()){
    char c = Wire.read();
    //Serial.print(c);
    if (c != ' '){
        respuesta[i]=c;
        i = i+1;
    }
  }
  i=1;
  ///----------------------------------------------
    while(Serial.available()>0) {
       char ch1 = Serial.read();
       //Serial.print(ch);
       if (ch1 != ' '){
        respuesta_mks[i]=ch1;
        i = i+1;
       }
       delay(10);
    }
   i=1;

  ///------------------------------------------
  
  
  delay(20);
  //Serial.println(dt);
  thing.handle();
  t2=millis();
  salidas();
  
}
