#include <SoftwareSerial.h>
#include <stdio.h>
#include <stdlib.h>
SoftwareSerial esp(8, 9);
SoftwareSerial presion(10,11);
SoftwareSerial pump(2,3);
 
bool turbo=false;
bool arduino;
unsigned long t1=0;
unsigned long t2=0;
char respuesta_pump[80];
char respuesta[50];
char encendido_bomba[3];
char tbp;
int seleccion=0; //para el uso del timer
int equipo; //equipos con los que se trabaja
int activo; //equipo activo en determinado momento
int i2c;
int valor_sonda;
float voltaje_sonda;
static int dig_bomba[6]={6,7,8,9,10,11};//{5,27,28,29,30,31};
static int dig_presion[6]={7,9,10,11,12};
/*Datos*/
String dt2;
String dt1;
String dtsend;
/*Equipo*/
String a="a";
String b="b";
String c= "c";
String alarma="d";
void setup() {
  t1=millis();
  Serial.begin(9600);  // put your setup code here, to run once:
  esp.begin(9600);
  pump.begin(9600);
  presion.begin(9600);

}
void conversion(char respuestas[],String equipo_){
  String pump_state(respuestas[1]);
  if (equipo_ == "a"){
  String d1(respuestas[8]);
  String d2(respuestas[10]);
  String d3(respuestas[11]);
  String d5(respuestas[13]);
  String d4(respuestas[14]);
  dt1 = (equipo_+d1+d2+d3+d5+d4);
  Serial.println(dt1);
  }
  else{
    String d1(respuestas[8]);
    if (d1=="1"){turbo =true; //Verifica que la sonda este encendida
      String d2(respuestas[9]);//on
      String d3(respuestas[10]);//speed reached
      String d5(respuestas[11]);//standby
      String d4(respuestas[27]);//respuesta velocidad
      String d6(respuestas[28]);
      String d7(respuestas[29]);
      String d8(respuestas[30]);
      String d9(respuestas[31]);
      //String d10(rand());
      dt2 = (equipo_+d4+d6+d7+d8+d9);
    }
    else{turbo=false;
      dt2="b00000";
    }
    Serial.println(dt2);
    
  }
  
  delay(10);
  
}
void medir(){
  if (arduino==true){
    //Medir de la sonda
    valor_sonda=analogRead(A1);
    voltaje_sonda=valor_sonda * (5.0 / 1023.0 );
    String medicion_sonda(voltaje_sonda);
    Serial.println(medicion_sonda);
    //Enviar mediciones
    esp.listen();
    esp.println(c + medicion_sonda);
    delay(50);
    i2c=i2c+1;
    esp.println(dtsend);
    arduino=false;
  }
  if((t2-t1)>= 1000){
    if (seleccion == 1){
      pump.listen();
      conversion(respuesta,a);
      equipo=1;
      pump.println("#007STA");
      delay(80);
      seleccion = 2;
      
      
    }
    else{
      presion.listen();
      conversion(respuesta_pump,b);
      equipo=2;
      presion.println("@253PR1?;FF");
      delay(80);
      //Serial.println("(2)");
      seleccion=1;
      
    }

    
  t1=t2;
  arduino=true;
  }
}


void loop() {
  int i=1;

    //Serial.println("?RT");
/*Lectura bomba turbo*/
  //pump.listen();
   while (0 <esp.available()) {
    tbp = esp.read();
    Serial.print(tbp);
    delay(10);
    if (tbp == 'f'){
        pump.println("#007TMPOFF");
        delay(10);
      }
     if (tbp == 'n'){
        pump.println("#007TMPON");
        delay(10);
      }
    
    }
    i=1;
  while(pump.available()>0) {
     char ch = pump.read();
     if (ch != ' '){
      respuesta_pump[i]=ch;
     }
     //Serial.print(ch);
     delay(10);
     //mySerial.write(ch);
     delay(1); 
     i = i+1; 
  }
  //delay(100);
  i=1;
  //conversion(respuesta_pump,b);
  //mySerial.println(dt2);
  
  
  //presion.listen();
/*Lectura presion*/
    while(presion.available()>0) {
     char ch = presion.read();
     if (ch != ' '){
      respuesta[i]=ch;
      //Serial.print(ch);
     }
     //mySerial.println(ch);
     //mySerial.println("recibido>");
     delay(10);
     delay(1); 
     i = i+1;  
  }
  i=0;

  
  
  //delay(100);
  //conversion(respuesta,a);
  
  t2=millis();
  medir();
  /*Seleccion i2c data*/
  if (i2c % 2 ==1){
    dtsend=dt1;
  }
  else{
    dtsend=dt2;
  }
  // put your main code here, to run repeatedly:

}
