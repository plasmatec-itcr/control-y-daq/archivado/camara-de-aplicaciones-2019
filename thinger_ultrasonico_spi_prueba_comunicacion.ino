#include <ESP8266WiFi.h>
#include <ThingerESP8266.h>
#include <X9C.h>
#include <stdio.h>
#include <stdlib.h>
#include <SoftwareSerial.h>

/*Credenciales Thinger.io*/
#define USERNAME "EduardoBadilla"
#define DEVICE_ID "ESP8266"
#define DEVICE_CREDENTIAL "abcdef123456"

#define SSID "Mario"//"AndroidAP"//"Mario"//"belkin.3433"
#define SSID_PASSWORD "22697052"//"rzzf9130"//"22697052"//"2433966a"

/*Potenciometro Digital*/
#define INC D5//2   // D1 Mini D4 - pulled up in H/W (10k) ->  chip pin 1
#define UD D6//15   // D1 Mini D8                          ->  chip pin 2
#define CS D7//16 
ThingerESP8266 thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);
SoftwareSerial Arduino(D3,D4);
SoftwareSerial Prueba(D1,D2);
X9C pot;
String mks;
char buff[10];
char respuesta[50];
char respuesta_buena[50];//Almacena la respuesta en caso de conexion fallida
char respuesta_mks[30];
bool activado=false;
bool up_conversion=false;
int i=1;
int indice;
int resistencia=0;
String dt;
String mks_flujo;
String mks_setpoint;
unsigned long presion;
unsigned long presion_ant;
unsigned long presiontot;
int voltaje_sonda;
int voltaje_sonda_ant;
//int vs1;
//int vs2;
//int vs3;
int vel_turbo=0;
int vel_turbo_ant=0; 
int intentos=5;
unsigned long t1=0;
unsigned long t2=0;
unsigned long t3=0;
unsigned long t4=0;
/*Contador y variables de salidas*/
int out_cnt=0;
bool activar_setpoint;
bool activar_setpoint_ant;
int setpoint=5;
int activar_bomba=0;
bool activar_bomba_boton;
bool activar_bomba_ant;
//CONTROL DEL RECIBIDO DE MENSAJES
//Presion=1, Bomba=2, MKS=3;
int equipo=0;
int exitosas=0;
int fallidas=0;
int conversiones=0;
int fallidas_seguidas=0;
bool testing=false;
bool correcta=false;
bool com_bomba=false;
bool com_sonda=false;
bool com_presion=false;
bool alerta_bomba=false;


void conversion1(){
  correcta=false;//verificador de respuestas utiles
  int mult_idx;
  //respuesta ya va a ser procesada
  String d1(respuesta_buena[1]);
  String d2(respuesta_buena[2]);
  String d3(respuesta_buena[3]);
  String d4(respuesta_buena[4]);
  String d5(respuesta_buena[5]);
  String d6(respuesta_buena[6]);
  Serial.println(d1);
  delay(20);
  if (d1=="a"){
    conversiones++;
    dt=(d2+d3+d4);
    presion=dt.toInt();
    indice= d6.toInt();
    //d5="+"; FIXME: Flag used only for simulator 
    mult_idx=(pow(10,indice));
    if (d5=="+"){
      presiontot=(presion*100)*mult_idx;//1000:FIXME ESCALA NO ES CORRECTA:CHECK 
      if( presion_ant*0.01 >= presiontot){
        presiontot=presion_ant;
      }  
      presion_ant=presiontot;
      thing.stream(thing["Presion"]);   
    }
    else if(d5 == "-"){
      presiontot=(presion*10) / mult_idx;//1000:FIXME
      if( presion_ant*0.01 >= presiontot){
        presiontot=presion_ant;
      }
      thing.stream(thing["Presion"]);
      presion_ant=presiontot;
    }
  }
  if (d1=="b"){
    conversiones++;
    dt=(d2+d3+d4+d5+d6); 
    vel_turbo=dt.toInt(); 
    /*Check emcendido de bomba*/
    if (vel_turbo != 0){
      activar_bomba=0; 
    }
    if((vel_turbo <= 0.11*vel_turbo_ant)){//FIXME
      vel_turbo=vel_turbo_ant;
    }
    else if (vel_turbo <= 100){
      vel_turbo=0;
    }
    thing.stream(thing["Velocidad"]);//VERIFY
    vel_turbo_ant=vel_turbo;
  }
  if(d1=="c"){
    conversiones++;
    dt=(d2+d4+d5);
    voltaje_sonda=dt.toInt();
    thing.stream(thing["Sonda"]);//VERIFY
    }
    /*reinicia las variables*/
//    for (int x=1; x >= sizeof(respuesta_buena);x++){
//      respuesta_buena[x]=' ';
//    }
}
void conversion2(){
  String d1mks(respuesta_mks[1]);
  String d2mks(respuesta_mks[2]);
  String d3mks(respuesta_mks[3]);
  String d4mks(respuesta_mks[4]);
  String d5mks(respuesta_mks[5]);
  delay(20);
  if((out_cnt % 2) == 0){
    mks_flujo=(d1mks+d2mks+d3mks+d4mks+d5mks);
  }
  else{
    mks_setpoint=(d1mks+d2mks+d3mks+d4mks+d5mks);
  }
  out_cnt++;
  //Serial.println(mks_flujo);
}
void setup() {
  t1=millis();
  t4=t1;
  Serial.begin(9600, SERIAL_7O1);
  Arduino.begin(9600);
  Prueba.begin(9600);
  pot.begin(CS,INC,UD);
  //Wire.begin(D1, D2);
  
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(D0, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  pinMode(13, OUTPUT);//trigger
  digitalWrite(13,LOW);
  
  //THINGER.IO
  thing.add_wifi(SSID, SSID_PASSWORD);
  thing["Flujo"] >> [] (pson& out){
    conversion2();
    out=mks_flujo;
  };
  // digital pin control example (i.e. turning on/off a light, a relay, configuring a parameter, etc)
  thing["Activar Bomba"] << digitalPin(D0);

/*Seteo MFC*/
  thing["Activar Setpoint MFC"] << [] (pson& in){
    if(in.is_empty()){
        in = activar_setpoint;
    }
    else{
        activar_setpoint = in;
    }
};
/*Ajuste MFC*/
  thing["Definir Setpoint MFC"] << [] (pson& in){
      if(in.is_empty()){
        in = setpoint;
    }
    else{
        setpoint = in;
    }
};
  thing["Voltaje Fuente"] << inputValue(resistencia,{
    pot.setPot(resistencia,true);
    delay(10);
  });
//  thing["Activar"] << [] (pson& in){ //FIXME verificar que se puedan controlar bidireccionalemente
//    in=activar_bomba_boton;
//    in=activar_setpoint; 
//    in=setpoint;
//    if(activar_bomba_boton == true &&  activar_bomba_ant == false){
//      activar_bomba = 1;
//    }
//    if(activar_bomba_boton == false && activar_bomba_ant == true){
//      activar_bomba = 2;
//    }
//    else{
//      activar_bomba == 0; 
//    }
//    activar_bomba_ant=activar_bomba_boton;
//  };

  thing["Camara"]>> [](pson& out){
    out["Flujo"]=mks_flujo;
    out["Setpoint"]=mks_setpoint;
    out["Presion"]=presiontot;
    out["Velocidad"]=vel_turbo;
    out["Tiempo ON"]=millis();
    out["Sonda"]=voltaje_sonda;
  };
  thing["Alerta"]>>[](pson& out){ //FIXME Add alertas
    //  Alerta de mantenimiento
    out["Comunicacion Bomba"]=com_bomba;
    out["Comunicacion Presion"]=com_presion;
    out["Comunicacion Sonda"]=com_sonda;//just because
    out["Alerta Bomba"]=alerta_bomba;
  };
  // more details at http://docs.thinger.io/arduino/
}
void MKS(){
  //MEDIR EL CANAL Y EL SETPOINT
  //funcion para medir flujo
  if((out_cnt % 2) == 0){
    Serial.println("AV1");//Valor del canal
  }
  else{
    Serial.println("?SP1");//Valor del setpoint
  }
  //Requiere encenderse
}
void salidas(){
  //FIXME: COMPLETAR SALIDAS PARA DEMAS FUNCIONES
  /*Set point MKS*/
  /*Set Speed Pump
   * Turn on/off Pump
   */
  if((t2-t1) >= 1800){
    /*Medicion de velocidad para determinar el estado de la bomba*/
    if(vel_turbo == 0){
      digitalWrite(D1, LOW);
    }
    else{
      
    }
    t1=t2;
    MKS();
    //delay(50);
//    if(activar_bomba == 1){
//        Arduino.println("d");  /* envio encendido bomba*///FIXME enviar hasta notar que la velocidad aumenta... puede generar problemas de loop 
//        //si hay algun impedimento para que la bomba reciba la se;al
//    }
//    else if (activar_bomba == 2){ /*envio apagado
//      Si es igual a 0 no hace nada*/
//        Arduino.println("e"); 
//    }
    if((activar_setpoint == true)&&(activar_setpoint_ant == false)){
        Serial.print("SP1,");  /* envio de setpoint*/
        Serial.print(String(setpoint)); //FIXME: CONVERTIR A STRING  
        Serial.println(".0");
    }
    /*Desactivar setpoint-Default value*/
    /*FIXME: Revisar q desactive el setpoint*/
    else if ((activar_setpoint == false)&&(activar_setpoint_ant == true)){
        Serial.println("DF,F");
    }
    activar_setpoint_ant=activar_setpoint;
  }
  
}

void loop() {
  Arduino.listen();
  //FIXME VARIABLES DEBEN DE REINICIALIZARSE PARA EVITAR CAER EN ERROR
  /*Leer los datos del microcontrolador*/
  while(Arduino.available()){//Arduino is for microcontroller, later on was changed for attiny84
    char c = Arduino.read();
    Prueba.print(c);
    if (c != ' '){
        if (c == 'p'){
          Prueba.println("--test run--");
          testing = true;
        }
        respuesta[i]=c;
        if (i == 1){
          //Serial.println(respuesta[1]);
          if ((respuesta[i] == 'a') || (respuesta[i] == 'b') || (respuesta[i] == 'c')){
            exitosas++;
            fallidas_seguidas=0;
            correcta=true;
            //respuesta_buena[1]=respuesta[1]; no hace falta. 
          }
          else{
            correcta=false; 
            /*FIXME*/
            /*-Llevar contador de respuestas desechadas
             * -Emitir alerta en caso de que se detecte cierta cantidad de respeustas erroneas consecutivas. DONE
             * -endpoint alerta de comunicacion
             */
            fallidas++;
            fallidas_seguidas++;
            if(fallidas_seguidas >= 10){
              //ALERTA
              com_bomba=true;
              com_sonda=true;
              com_presion=true;
              thing.call_endpoint("endpoint",thing["Alerta"]); 
              /*FIXME
               * definir alertas individualizadas por canal
               */
               
              fallidas_seguidas=0; 
            }
            //break; FIXME> CHECK SI CAUSA PROBLEMAS CON LS RESPUESTAS
          }
        }
        if(correcta == true){
          respuesta_buena[i]=c;
        }
        i = i+1;
    }
    up_conversion=true;
  }
  
  i=1;
  if (up_conversion==true){
    conversion1();
    up_conversion=false;    
  }
  ///----------------------------------------------
    while(Serial.available()>0) {
       char ch1 = Serial.read();
       
       if (ch1 != ' '){
        //Serial.println(ch1);
        respuesta_mks[i]=ch1;
        i = i+1;
       }
       delay(10);
    }
   i=1;

  ///------------------------------------------
  
  
  delay(20);
  //Serial.println(dt);
  thing.handle();
  t2=millis();
  t3=t2;
  if (testing == true){
    if (respuesta[1] == 'z'){
      t4=t3;
      testing = false;
      Prueba.listen();
      Prueba.println("---resultados   prueba---");
      Prueba.print("exitosas> ");
      Prueba.println(exitosas);
      Prueba.print("exitosas % > ");
      Prueba.println((100*exitosas)/conversiones);
      Prueba.print("fallidas> ");
      Prueba.println(fallidas);
      Prueba.print("conversiones> ");
      Prueba.println(conversiones);
      exitosas=0;
      fallidas=0;
      conversiones=0;
      delay(50);
    }
  }
  salidas();
  
}
