#include <SoftwareSerial.h>
#include <stdio.h>
#include <stdlib.h>
SoftwareSerial mySerial(5, 4);
float A=0;
int cont_pend=1;
int conta =1;
long t1=0;
long t2=0;
long tiempo=1;
long tiempoX=0;
bool pico =false;
bool periodo_fin=false;
bool estado_actual;
bool estado_anterior;
float medx,medy=0;
float Num, Num2, Den,Den2 =1;
volatile float pendiente_final;
volatile float pendiente;
/*Maxima corriente medida con Multimetro Agilent 34405a
 * Gas:Nitrogeno
 * A 100V+ I=0.17 A max
 * Para un plasma Glow discharge
 * Presion:6.95-2
 * Flujo:5 SCCM
 * Fuente: Voltaje= 500V  Corriente= 20 mA
 * Max medicion de corriente registrada traducida a voltaje:2.92
 */
void setup() {
  // put your setup code here, to run once:
  pinMode(4, OUTPUT);
  pinMode(5, INPUT);
  pinMode(7,INPUT);
  mySerial.begin(9600);
  t1 =millis();
  mySerial.println("Sonda Start");
}

void loop() {

 volatile float medicion=0;
 volatile float medicion_fuente=0;
 volatile float medicion_buffer[40];
 volatile float medicion_buffer_fuente[40];
 volatile float xy_buffer[40];
 volatile float x2_buffer[40];
 volatile float max_fuente=0;//Encuentra el valor pico
 volatile float medX;
 volatile float medY;
 volatile float medXY;
 volatile float medX2;

  medicion=analogRead( 6 ) * (5.0 / 1023.0 );
  /*Medicion Fuente*/
  medicion_fuente=analogRead( 7 )  * (5.0 / 1023.0 );
  /*Revision maximo*/
  if (medicion_fuente > max_fuente){ 
    max_fuente=medicion_fuente; 
  }
  /*Revision de flanco*///FIXME
  if (medicion_fuente >= 0.9*max_fuente){
    pico =true;
  }
  estado_actual=digitalRead(7);
  if((estado_actual != estado_anterior)&&(estado_actual == LOW)){
    //mide tiempo
    //FIXME EN ATTINY no esta detectando picos
    t2=millis();
    tiempo=t2-t1;
    t1=t2;
    pico =false; 
    periodo_fin=true;
    mySerial.println("flanco");
  }
    estado_anterior=estado_actual;
    tiempo=3000;//FIXME
    medicion=medicion - 1.1;
    /*conversion a mA*/
    medicion=58.22*medicion;
    t2=millis();
    tiempoX=t2-t1;
    
    if(tiempoX >= tiempo){//FIXME>quick workaround para picos
      t1=t2;
      pico =false; 
      periodo_fin=true;
    }
    A=(120000 / tiempo);//100/tiempo
//    B=-20;//min_fuente;//-20
    medicion_fuente=(A*(tiempoX)/1000)-20;

/*Obtencion de pendiente y parametros del plasma
 * Aproximacion de pendiente por minimos cuadrados
 * 
*/

if ((tiempoX >= (tiempo*0.5)) && (tiempoX <= (tiempo*0.55))){//FIXME 0.55
    medicion_buffer[conta]=medicion;
    medicion_buffer_fuente[conta]=medicion_fuente;
    xy_buffer[conta]=medicion*medicion_fuente;
    x2_buffer[conta]=pow(medicion,2);
    conta++;
}
/*cuando termina el periodo calcula*/

if (periodo_fin == true){
    //Suma X
    for (int x=1; x < conta; x++){
      medX=medX + medicion_buffer_fuente[x];
      medY=medY + medicion_buffer[x];
      medXY=medXY + xy_buffer[x];
      medX2=medX2 + x2_buffer[x];
      }
    /*Calculo de la temperatura (pendiente)*/
    int n=conta-1;
    Num= medXY - ((medx / n)*(medy / n));
    Den= medX2 -((pow(medx,2)/n));
    conta=1;
    pendiente= (Num / Den );
    //mySerial.println(pendiente);
    medX=0;
    medY=0;
    medXY=0;
    medX2=0;
    periodo_fin=false;
    mySerial.println(pendiente);
  }
  delay(200);
      /*Grafica*/
}
